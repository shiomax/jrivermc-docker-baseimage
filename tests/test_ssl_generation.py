from os import environ
from testcontainers.core.container import DockerContainer
from testutils import wait_for_container_ready

image_name=environ.get('TEST_IMAGE', default = None)

# Path to certificate file(s) in container
CERT_FILE="/config/certs/fullchain.pem"
SECRET_FILE="/config/certs/privkey.pem"
# 7 days in seconds time, certs only valid for that long are considered expired
EXPIRY_TIME="604800"

def test_secure_connection():
    """
    Tests if SSL certs are generated when SECURE_CONNECTION flag is set.
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "1").with_exposed_ports(5800) as container:
        wait_for_container_ready(container)

        resultExists = container.exec(f"test -f {CERT_FILE}")

        assert resultExists.exit_code == 0, "Certificate file does not exist"

        resultExpiryCheck = container.exec(f"openssl x509 -noout -in {CERT_FILE} -checkend {EXPIRY_TIME}")

        if 'Certificate will expire' in str(resultExpiryCheck.output):
            assert False, "Certificate is expired"
