#! /usr/bin/python3

from time import sleep
from typing import Callable
from testcontainers.core.container import DockerContainer
import requests
import traceback

def wait_for_http_get_ok(url :str, timeout :int = 60) -> bool:
    """
    wait_for_http_get_ok(url, timeout = 60) -> bool

    Retries calling the specified GET url until the status code is 200 or a timeout is reached.
    Timeout defaults to 60 seconds.
    """
    return retry_with_timeout(lambda: requests.get(url).status_code == 200, timeout)

def retry_with_timeout(function: Callable, timeout :int = 60) -> bool:
    """
    retry_with_timeout(function, timeout = 60) -> bool

    Retries a function that returns a bool until it returns True or a timeout is reached.
    Timeout defaults to 60 seconds.
    """
    while timeout > 0:
        try:
            response = function()
            if response: return True
        except Exception:
            traceback.print_exc()
            pass
        finally:
            timeout -= 1
            sleep(1)
    return False

def wait_for_file_exists(container :DockerContainer, path: str, timeout :int = 60) -> bool:
    """
    Waits for file to exist inside docker container
    """
    while timeout > 0:
        try:
            result = container.exec(f"test -f {path}")
            if result.exit_code == 0: return True
        finally:
            timeout -= 1
            sleep(1)
    return False

def wait_for_container_ready(container :DockerContainer) -> bool:
    """
    Waits services in container to be up.
    """
    if not wait_for_file_exists(container, "/tmp/webapi.sock"): return False
    if not wait_for_file_exists(container, "/config/appstorage/db.sqlite"): return False

    init_url = f"http://{container.get_container_host_ip()}:{container.get_exposed_port(5800)}/api/general/init"
    if not wait_for_http_get_ok(init_url): return False
    return True