#!/bin/bash

ENV_NAME="testenv"
TEST_IMAGE="${TEMP_IMAGE_NAME:-unset}"
TEST_FILE=""

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        # Specify image to use to test
        --image)
            shift
            TEST_IMAGE="$1"
            ;;
        # Specify single file to test, skips other tests
        --test-file)
            shift
            TEST_FILE="$1"
            ;;
        -*)
            usage "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

if [ "$TEST_IMAGE" = "unset" ]; then
    echo "Image to test was not set. Use --image <image_name> to define when not running in CI."
    exit 1
fi

# Export image to test
export TEST_IMAGE

echo "Running tests..."

[ -z "$( which python3 )" ] && echo "ERROR: To run test, python3 needs to be installed." && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

CREATED_VENV=0

# Create virtual env
[ -d "$SCRIPT_DIR/$ENV_NAME" ] || { python3 -m venv "$SCRIPT_DIR/$ENV_NAME"; CREATED_VENV=1; }

# Activate virtual env
if [ "$VIRTUAL_ENV" == "" ] || [ "$(basename -- $VIRTUAL_ENV)" != "$ENV_NAME" ]; then
    source "$SCRIPT_DIR/$ENV_NAME/bin/activate"
fi

# Install requirements on newly created venv
if ! [ $CREATED_VENV -eq 0 ]; then
    # Fix issue with pkg_resources==0.0.0 being inside requirements.txt
    # It should never be in there it´s part of python setuptools and won´t install successfully
    cat "$SCRIPT_DIR/requirements.txt" | grep --invert-match pkg-resources | xargs -n 1 pip install
fi

# Set Python Path to include modules in rootfs
export PYTHONPATH="$SCRIPT_DIR/../rootfs/opt/websockify"

# Run Tests
if ! ( cd $SCRIPT_DIR ; pytest "$TEST_FILE" ); then
    echo "One or more tests failed. Exiting..."
    exit 1
fi

echo "Tests completed successfully!"
