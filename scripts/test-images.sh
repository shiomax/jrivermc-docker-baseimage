#!/bin/bash

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        --arch)
            shift
            ARCH="$1"
            ;;
        -*)
            echo "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

IMAGE_NAME="$(./scripts/readvar.sh "$ARCH" TEMP_IMAGE_NAME)"
IMAGE_TAG="$(./scripts/readvar.sh "$ARCH" TEMP_IMAGE_TAG)"

./scripts/cleanup.sh "$IMAGE_NAME"

./scripts/load-images.sh --arch "$ARCH"

./tests/run.sh --image "$IMAGE_NAME:$IMAGE_TAG"

./scripts/cleanup.sh "$IMAGE_NAME"
