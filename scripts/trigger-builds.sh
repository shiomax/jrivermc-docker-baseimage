#!/bin/bash

BRANCH="$CI_COMMIT_REF_NAME"

if [ "$BRANCH" = "main" ]; then
    DOCKER_REPOSITORY="shiomax/jrivermc"
else
    DOCKER_REPOSITORY="shiomax/jrivermc-testing"
fi

echo "Triggering builds..."
echo "BRANCH: $BRANCH"
echo "REF NAME: $CI_COMMIT_REF_NAME"
echo "DOCKER_REPOSITORY: $DOCKER_REPOSITORY"
echo "TRIGGER URL: $CI_GITLAB_API_URL/projects/$CI_GITLAB_PROJECT_ID/trigger/pipeline"
echo "--------------------------\n"

print_bold() { echo -e "\033[1m$1\033[0m"; }

# Image exists if exit code is 0
docker_tag_exists() {
    curl --silent -f -lSL https://hub.docker.com/v2/repositories/$DOCKER_REPOSITORY/tags/$1 &> /dev/null
}

docker_tag_last_push() {
    ISO_TIME="$(curl --silent -X GET https://hub.docker.com/v2/repositories/$DOCKER_REPOSITORY/tags/$1 | jq --raw-output '.tag_last_pushed')"
    if [ -z $ISO_TIME ] || [ "$ISO_TIME" = "null" ]; then
        # Image does not exist just writing earliest time as a default
        echo "0"
    else
        # Convert ISO to UNIX seconds
        echo "$(date -d "$ISO_TIME" +%s)"
    fi
}

# Check if last update for image $1 is older than a week
docker_tag_needs_update() {
    CURRENT_TIME="$(date +%s)"
    LAST_PUSH="$(docker_tag_last_push $1)"

    DIFF_SECONDS="$(($CURRENT_TIME - $LAST_PUSH))"
    SECONDS_WEEK="$((60 * 60 * 24 * 7))"

    [ "$DIFF_SECONDS" -gt "$SECONDS_WEEK" ] && echo "1" || echo "0"
}

# $1 = JRIVER_RELEASE, $2 = JRIVER_TAG
trigger_deb_builds() {
    JRIVER_RELEASE="$1"
    JRIVER_TAG="$2"
    VARIANT="$3"
    LATEST_TRIGGERED="$4"
    STABLE_TRIGGERED="$5"

    while read line; do

        DEB_MC_VERSION="$(echo $line | awk '{print $2}')"
        IMAGE_VERSION="$(./scripts/get-image-version.sh)"
        
        DOCKER_TAG="$JRIVER_TAG-$DEB_MC_VERSION-$IMAGE_VERSION"
        echo "checking $DOCKER_TAG"

        if ! docker_tag_exists "$DOCKER_TAG" || ! docker_tag_exists "$DOCKER_TAG-amd64" || ! docker_tag_exists "$DOCKER_TAG-arm64"; then
            # Trigger latest when new version was found, but update was not triggered yet
            if [ "$LATEST_TRIGGERED" = "0" ]; then
                LATEST_TRIGGERED="1"
                trigger_latest "$VARIANT"
            fi

            if [ "$STABLE_TRIGGERED" = "0" ]; then
                STABLE_TRIGGERED="1"
                trigger_stable "$VARIANT"
            fi

            DEB_URL_AMD64="$(echo $line | awk '{print $3}')"
            DEB_URL_ARM64="$(echo $line | awk '{print $4}')"

            # Trigger deb_url build
            print_bold "Triggering rebuild for '$DOCKER_TAG'"

            curl -X POST --silent \
                -H "Content-Type: multipart/form-data" \
                -F "token=$CI_PIPELINE_TRIGGER" \
                -F "ref=$BRANCH" \
                -F "variables[CI_BUILD_VARIANT]=$variant" \
                -F "variables[JRIVER_TAG]=$JRIVER_TAG" \
                -F "variables[CI_DEB_URL_AMD64]=$DEB_URL_AMD64" \
                -F "variables[CI_DEB_URL_ARM64]=$DEB_URL_ARM64" \
                -F "variables[CI_RELEASE_REPOSITORY]=$DOCKER_REPOSITORY" \
                "$CI_GITLAB_API_URL/projects/$CI_GITLAB_PROJECT_ID/trigger/pipeline" > /dev/null

            # Only building one .deb url build for dev branches
            [ "$BRANCH" = "main" ] || { echo "Not on 'main' branch, will skip additional builds."; return 0; }
        else
            echo "Skipping '$DOCKER_TAG' already exists in repo."
        fi
    done < <(./scripts/find-versions.sh --arch amd64,arm64 --version "$JRIVER_RELEASE" --channel "v$JRIVER_RELEASE/$JRIVER_TAG" --blacklist ./scripts/versions_blacklist)
}

# $1 = variant
trigger_latest() {
    variant="$1"

    print_bold "Triggering latest build for $variant"

    # Trigger latest Build from repo
    curl -X POST --silent \
        -H "Content-Type: multipart/form-data" \
        -F "token=$CI_PIPELINE_TRIGGER" \
        -F "ref=$BRANCH" \
        -F "variables[CI_BUILD_VARIANT]=$variant" \
        -F "variables[JRIVER_TAG]=latest" \
        -F "variables[CI_RELEASE_REPOSITORY]=$DOCKER_REPOSITORY" \
        "$CI_GITLAB_API_URL/projects/$CI_GITLAB_PROJECT_ID/trigger/pipeline" > /dev/null

    [ $? -eq 0 ] || echo "Triggering latest build failed."
}

# $1 = variant
trigger_stable() {
    variant="$1"

    print_bold "Triggering stable build for $variant"

    # Trigger stable build from repo
    curl -X POST --silent \
        -H "Content-Type: multipart/form-data" \
        -F "token=$CI_PIPELINE_TRIGGER" \
        -F "ref=$BRANCH" \
        -F "variables[CI_BUILD_VARIANT]=$variant" \
        -F "variables[JRIVER_TAG]=stable" \
        -F "variables[CI_RELEASE_REPOSITORY]=$DOCKER_REPOSITORY" \
        "$CI_GITLAB_API_URL/projects/$CI_GITLAB_PROJECT_ID/trigger/pipeline" > /dev/null

    [ $? -eq 0 ] || echo "Triggering stable build failed."
}

VARIANTS_TO_TRIGGER="3"
# Only build latest variant when not on main branch
[ "$BRANCH" = "main" ] || VARIANTS_TO_TRIGGER="1"

while read variant; do

    JRIVER_RELEASE="$(cat ./versions/$variant | grep "^JRIVER_RELEASE=.*$" | awk -F= '{print $2}')"

    # Has already been triggered don´t trigger again when new jriver version is found
    LATEST_TRIGGERED="0"
    STABLE_TRIGGERED="0"

    if [ "$BRANCH" != "main" ] || [ "$(docker_tag_needs_update "latest-$JRIVER_RELEASE")" = "1" ]; then
        LATEST_TRIGGERED="1"
        # Trigger latest when oler than 7 days or not on main branch
        trigger_latest "$variant"
    fi

    STABLE_AVAILABLE="$(cat ./versions/$variant | grep "^STABLE_AVAILABLE=.*$" | awk -F= '{print $2}')"
    echo "STABLE_AVAILABLE: $STABLE_AVAILABLE"

    if [ "$STABLE_AVAILABLE" = "1" ]; then
        # Trigger stable when older than 7 days or not on main branch
        if [ "$BRANCH" != "main" ] || [ "$(docker_tag_needs_update "stable-$JRIVER_RELEASE")" = "1" ]; then
            STABLE_TRIGGERED="1"
            trigger_stable "$variant"
        fi
    else
        # Just set to 1 to not trigger rebuild when finding unbuilt versions
        STABLE_TRIGGERED="1"
    fi

    # Trigger builds from .deb packages
    trigger_deb_builds $JRIVER_RELEASE "latest" "$variant" "$LATEST_TRIGGERED" "$STABLE_TRIGGERED"

# Building latest 3 variants available
done < <(ls -l ./versions | tail -n +2 | awk '{print $9}' | sort --reverse --sort=general-numeric | head -n "$VARIANTS_TO_TRIGGER")
