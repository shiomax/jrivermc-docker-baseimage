#!/bin/bash

set -e

usage() {
    if [ "$*" ]; then
        echo "$*"
        echo
    fi

    echo "usage: $( basename $0 ) [OPTIONS]

Options:
  -h, --help          Display this help.
  -c, --channel       Channel part of the url (default: v26/latest)
  -v, --version       JRiver version (default: 29)
  --arch              Architecture (default: amd64). Use comma seperated list to list multiple (only lists when both are available).
  --min               Min version for iterator (refers to last number of jriver version 26.0.0) (default: 0).
  --max               Max version for iterator (refers to last number of jriver version 26.0.200) (default: 200).
  --blacklist <file>  Supply a line break seperated blacklist of full version numbers (29.0.20 etc.) to exclude.
"
}

channel="v29/latest"
version=29
arch="amd64"
min_version=0
max_version=100
blacklist=""

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
            usage
            exit 0
            ;;
        -c|--channel)
            shift
            channel="$1"
            ;;
        -v|--version)
            shift
            version="$1"
            ;;
        --min)
            shift
            min_version="$1"
            ;;
        --max)
            shift
            max_version="$1"
            ;;
        --blacklist)
            shift
            [ -f $1 ] && blacklist="$(cat $1)" || { echo "Blacklist file $1 does not exist."; exit 1; }
            ;;
        --arch)
            shift
            arch="$1"
            if ! [[ "$arch" =~ ^(amd64|armhf|arm64)(,(amd64|armhf|arm64))*$ ]]; then
                echo "Invalid argument for --arch, valid architectures are amd64, armhf and arm64."
                echo "Supply comma seperated list for multiple."
                exit 1
            fi
            ;;
        -*)
            usage "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

for v in $(seq $min_version $max_version); do
    line=""
    for a in $(echo $arch | sed "s/,/ /g"); do
        url="https://files.jriver-cdn.com/mediacenter/channels/$channel/MediaCenter-$version.0.$v-$a.deb"
        
        status=$(curl --silent --head "$url" | head -n 1)

        if [ $(echo $status | grep --count "200") == 1 ]; then
            if [ "$blacklist" != "" ] && [ $(echo "$blacklist" | grep --count "$version.0.$v") -eq 1 ]; then
                line=""
                break;
            fi

            if [ "$line" = "" ]; then
                line="$version $version.0.$v $url"
            else
                line="$line $url"
            fi
        else
            line=""
            break
        fi
    done

    if [ "$line" != "" ]; then
        echo $line
    fi
done
