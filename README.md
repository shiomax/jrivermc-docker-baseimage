# Docker Image for JRiver Media Center

This is a community built docker image for [JRiver Media Center](https://jriver.com/)

The sourcecode is available on [gitlab.com](https://gitlab.com/shiomax/jrivermc-docker).

It's intended for use as a central 'headless' JRiver server you stream your media from with any of your other MC instances.

Direct music playback from the container (can) be setup, but is dependent on having alsa running on the host (see Sound Section).

# Screenshots

![screenshot](https://gitlab.com/shiomax/jrivermc-docker/raw/main/assets/screenshot.png)

# Available Versions

| MC Version | Versions Available |
| ---------- | ------------------ |
| ```33```   | ```latest``` only
| ```32```   | ```latest``` and ```stable```
| ```31```   | ```latest``` and ```stable```
| ```30```   | ```latest``` and ```stable```
| ```29```   | ```latest``` and ```stable```

Images are released on this [Docker Hub Repository](https://hub.docker.com/r/shiomax/jrivermc). All mediacenter version are in the same repository, check out the [Image Tags Section](#image-tags) for the tag format to pick the mediacenter version you want to use.

# How to use this

### **Docker Compose**

[docker-compose](https://docs.docker.com/compose/) is a great tool to get your container configuration maintainable. Instead of typing out the ```docker run``` and then forgetting what it was when you want to update the image months later you'll create a configuration file ```docker-compose.yml```.

Bridge networking (default)

```yml
version: '3'
services:
  jrivermc:
    image: shiomax/jrivermc:latest-33
    restart: always
    container_name: jrivermc
    mac_address: ed:e8:60:2d:65:c1
    ports:
      - "5800:5800"
      - "5900:5900"
      - "52100:52100"
      - "52101:52101"
      - "52199:52199"
      - "1900:1900/udp"
    volumes:
        - /path/to/config:/config:rw
        - /path/to/log:/log:rw
        - /path/to/music:/data/music:rw
```

Host networking

```yml
version: '3'
services:
  jrivermc:
    image: shiomax/jrivermc:latest-33
    restart: always
    container_name: jrivermc
    network_mode: host
    volumes:
        - /path/to/config:/config:rw
        - /path/to/log:/log:rw
        - /path/to/music:/data/music:rw
```

To run the container and detach from the output ```docker-compose up -d``` in the directory the ```docker-compose.yml``` resides. 

Updating the container
- ```docker-compose pull``` to pull the latest image as specified in the compose file
- ```docker-compose down``` to stop and remove the container (your data will stick around if you mounted something to ```/config```)
- ```docker-compose up -d``` to create the container and detach from the screen
- ```docker image prune``` cleanup unused images (docker is a giant disk space hog if you never remove old images they will stick around until the end of days)

### **Docker Run**

Bridge networking (default)

```sh
docker run -d \
    --name=jrivermc \
    --net=bridge \
    --restart=always \
    --mac-address=ed:e8:60:2d:65:c1 \
    -p 5800:5800 \
    -p 5900:5900 \
    -p 52199:52199 \
    -p 52101:52101 \
    -p 52100:52100 \
    -p 1900:1900/udp \
    -v /path/to/log:/log:rw  \
    -v /path/to/config:/config:rw \
    -v /path/to/music:/data/music:rw \
    shiomax/jrivermc:latest-33
```

Host networking

```sh
docker run -d \
    --name=jrivermc \
    --net=host \
    --restart=always \
    -v /path/to/log:/log:rw  \
    -v /path/to/config:/config:rw \
    -v /path/to/music:/data/music:rw \
    shiomax/jrivermc:latest-33
```

Updating the container
- ```docker pull shiomax/jrivermc:latest-33``` pull a new image with tag latest
- ```docker rm -f jrivermc``` stop and remove the container with name jrivermc (your data will stick around if you mounted something to ```/config```)
- ```docker run -d ....``` rerun the entier run command you ran to initially create the container
- ```docker image prune``` cleanup unused images (docker is a giant disk space hog if you never remove old images they will stick around until the end of days)

# Volumes (Bind Mounts)

### Config Directory
The directory ```/config``` within the container acts as the home directory for anything that should be persisted. Make sure you mount it to a directory on your host. If you do not your container will use a temporary home directory withing the containers file system that will be lost when updating the image.

### Log Directory
Logs of relevant services running in the container are written to ```/log```. If you want to persist logs or have easier access to the logs outside the container mount this directory to a directory on your host. Most of the logs are written by ```logutil-service``` from s6-overlay. Its logs everything that gets written to stdout or strerr to rotating files. The only exception currently is nginx which does it's own logging.

### Mount media
There are no predefined directories for where you should mount your media to. Anything goes as long as you do not override directories that are relevant for the system. Personally, I mounted all the media to ```/data/music``` and similar directories.

# Accessing the GUI

### Web GUI

The webui can be accessed by ```https://<HOST IP ADDR>:5800```

When using ```SECURE_CONNECTION=0``` to disable SSL/HTTPS use ```http://<HOST IP ADDR>:5800```. Note that novnc wants there to be a SSL certificate. At this time it works without SSL, but complains about it. Therefore it´s not recommended to turn this off unless you are going to manage SSL outside the container.

Most modern browsers should work, but I'm only trying if it works with firefox, chrome and safari specifically.

Upon first accessing the Web GUI you'll get to an "adoption" screen to create the first user. This can only be done once. The username and password can be changed later. If desired authentication can also be disabled by setting ```DISABLE_AUTH``` to ```1```.

### VNC

As authentication is currently not set up for direct vnc connections and in the short term there are no plans to change that VNC by default does not listen on any port at all, thus making it completely unavailable for regular VNC clients. Setting ```VNC_LISTEN``` to ```LOCALHOST``` or ```ALLOW_ALL``` will enable the VNC server on port ```5900``` with ```LOCALHOST``` restricting connections to localhost only.

To connect to VNC use the following URL: ```<HOST IP ADDR>:5900```

# Ports

Ports:
- Ports used by MC: 52100, 52101, 52199, 1900/udp
- Port for Web GUI: 5800
- Port for VNC access: 5900 (only available when ```VNC_LISTEN``` is set to ```LOCALHOST``` or ```ALLOW_ALL```, as of now there is no authentication setup for direct VNC connections)

# Sound

You can map your alsa sound device ```/dev/snd``` between the host and container to enable sound playback from within the container.

- When using the docker run command ```--device /dev/snd``` as an option.

- When using ```docker-compose``` add the following to your ```docker-compose.yml```.

```yml
devices:
  - "/dev/snd"
```

# CDROM

Mapping cdrom devices works the same as with sound devices.

- When using docker run command ```--device /dev/sr0```

- When using ```docker-compose``` add the following to your ```docker-compose.yml```.

```yml
devices:
  - "/dev/sr0"
```

Mediacenter only supports cdrom drives since version 30.0.44. The initial changelogs state that it has to be ```/dev/sr0``` and will not look for secondary drives. If you have multiple and want to use ```/dev/sr1``` in the container you can remap it to ```/dev/sr0``` by using ```/dev/sr1:/dev/sr0```.

# Permission Problems

If you are having trouble with permissions in the container. Changing permissions on the files would be one way to solve that, but you can also change what user mediacenter runs as in the container by setting the ```GROUP_ID``` and/or ```USER_ID``` environment variables to one that does have access. By default both are set to ```1000```. On a generic Linux install thats the first User you end up with.

The two are numeric values. Use ```ls -la``` in one of your media directories to figure out what user / group it belongs to.

The output may look something like this

```bash
root@box:/data/music/example# ls -la
total 422693
drwxr-xr-x 2 user group        0 Aug 28 14:28  .
drwxr-xr-x 2 user group        0 Feb 13  2022  ..
-rwxr-xr-x 1 user group 34253407 Aug 28 14:28 'song 1.flac'
-rwxr-xr-x 1 user group 31919237 Aug 28 14:28 'song 2.flac'
-rwxr-xr-x 1 user group 37532001 Aug 28 14:28 'song 3.flac'
```

You can do this within the container or outside the container. The user and group columns may be names or ids. Inside the container the user the container is running as is always named ```app```. User and group names inside the container and the host will not necessarily match (most of the time they won´t). But when the numeric values match up it will be considered effectively the same user / group.

To get the numeric id for a user or group with the name use ```id <name>```.

To set those environment variables using docker run use the following options

```
-e USER_ID=<user_id>
-e GROUP_ID=<group_id>
```

For ```docker-compose``` add them to the environment section in your ```docker-compose.yml```

```yml
version: '3'
services:
  jrivermc:
    environment:
      - USER_ID=<user_id>
      - GROUP_ID=<group_id>
```

### What if my media is owned by root:root?

You should not configure the container to run mediacenter as root user. Instead change ownership of the files.

Use ```chown user:group <path_to_file>``` to change ownership on a single file. Or ```chown -R user:group <path_to_folder>``` to change ownership on an entier folder including it´s contents.

### But... I still can´t access the files?

If you still do not have access to your media you´ll have to look at the file permissions. In the ```ls -la``` output it repeats ```rwx``` (read, write and execute) 3 times for user, group and others. The dashes are permissions that are not given.

Use ```chmod <permission_modifier> <path_to_file>``` to change permissions on a single file. Or ```chmod -R <permission_modifier> <path_to_folder>``` to change permissions on an entier folder including it´s contents.

As permission modifier a 3 digit number can be used. Such as ```750```. The first number being for the user, second for the group and third for all others.

To calculate the exact numbers you want to add up ```4``` for read, ```2``` for write, and ```1``` execute.

```750``` would be equivalent to the user being granted 4+2+1=7 (all permissions) the group being granted 4+1=5(read and execute) and other being granted no permissions. ```750``` is probably fine for most cases. You don´t really need the execute permissions on media files, however you do need execute permissions on folders to be allowed to list their contents.

If you want to be more granular about it you can use the ```find``` command to filter out directories or files and set files to ```640``` and directories to ```750```
```bash
chmod 750 $(find <path_to_folder> -type d)
chmod 640 $(find <path_to_folder> -type f)
```

# Hardware Acceleration (experimental)

### NVIDIA

Prerequisites:
  - NVIDIA Driver installed on the host machine
  - NVIDIA Container Toolkit installed on the host machine ([Get Started Link](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#getting-started))

Start the container using the following parameters ```--gpus all```

The required environment variables ```NVIDIA_DRIVER_CAPABILITIES=all``` and ```NVIDIA_VISIBLE_DEVICES=all``` are already set. ```NVIDIA_VISIBLE_DEVICES``` can limit access to a specific card when specifying the UUID of the card (can be found by running ```nvidia-smi --query-gpu=gpu_name,gpu_uuid --format=csv```)

### INTEL

- When docker run command add the following options

```
--device /dev/dri/renderD128:/dev/dri/renderD128
--device /dev/dri/card0:/dev/dri/card0
```

- When using ```docker-compose``` add the following to your ```docker-compose.yml```.

```yml
devices:
  - "/dev/dri/renderD128"
  - "/dev/dri/card0"
```

If it fails to detect that you have an INTEL gpu, you can add ```-e INSTALL_INTEL_GPU_DRIVER=1``` to install the drivers anyways.

The devices ```/dev/dri/renderD128``` and ```/dev/dri/card0``` have to be owned by a group. Any group that is not equal to ```0``` (root user).

If the container sais on startup that one of those devices is owned by root you can fix this by adding a group to those device files.

You can check permissions with ```ls -la /dev/dri/``` on your host.

```bash
max@fax:~ $ ls -la /dev/dri/
total 0
drwxr-xr-x   3 root root        100 Jun 17 17:40 .
drwxr-xr-x  21 root root       4880 Jun 17 17:52 ..
drwxr-xr-x   2 root root         80 Jun 17 17:40 by-path
crw-rw----+  1 root video  226,   0 Jun 17 17:40 card0
crw-rw-rw-   1 root root   226, 128 Jun 17 17:40 renderD128
```

Above ```/dev/dri/renderD128``` is owned by ```root:root``` run the following to add it to the ```render``` group.

```bash
sudo groupadd render
sudo chown :render /dev/dri/renderD128
```

And do the same for ```/dev/dri/card0``` with group ```video``` if that one is missing.

It does not matter what group they are owned by. Just needs to be some group so the container can add that group to the jriver user on startup. The group names ```video``` and ```render``` are just suggestions as they seem to be the default.

# Container commands

There are some extra commands / scripts available in the container that do various things.

| Command | Description |
| ------- | ----------- |
| ```mcd-stop``` | Stops mc and prevents it from starting again. |
| ```mcd-start``` | Removes lock that prevents mc from auto restarting. |
| ```mcd-activate <.mjr file path>``` | Stops mc, applies activation file, starts mc again. This is the same script that can be triggered from the webgui in the Activation section. |

# Activate MC using .mjr file

There are a few extra steps necessary to activate using a ```.mjr``` file. You can either use the Activation section in the GUI or use the ```mcd-activate <.mjr file path>``` command inside the container directly to apply the ```.mjr``` file. If you want to know specifically what this does you can look at the script [here](https://gitlab.com/shiomax/jrivermc-docker/-/blob/main/rootfs/usr/bin/mcd-activate).

# Firewall-Configuration

Usually only necessary for host networking. With a regular docker install all ports forwarded in bridge networking are already allowed to go threw the firewall.

### firewalld (centos, fedora or rhel)

1. Login as root ```sudo su``` (ALL of these things need root access, or don't and prepend sudo to every command)
2. Create a file ```nano /etc/firewalld/services/jriver.xml```
```xml
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>jriver</short>
  <description>Ports required by JRiver MediaCenter.</description>
  <port protocol="tcp" port="5800"></port>
  <port protocol="tcp" port="52199"></port>
  <port protocol="tcp" port="52100"></port>
  <port protocol="tcp" port="52101"></port>
  <port protocol="tcp" port="5900"></port>
  <port protocol="udp" port="1900"></port>
</service>
```
3. Reload firewalld ```firewall-cmd --reload```
4. Add configuration temporarily ```firewall-cmd --add-service=jriver --zone=public```
5. Try if you can access jriver
6. If yes, make the configuration permanent 
    - ```firewall-cmd --add-service=jriver --zone=public --permanent```
    - ```firewall-cmd --reload```
7. If no, you'll have to figure that out. Since the configuration was only temporarily applied another reload will make it go away.

### ufw (ubuntu or debian *if you install ufw*)

1. Login as root ```sudo su``` or ```su``` on debain (ALL of these things need root access, otherwise prepend sudo to every command)
2. Create a file ```nano /etc/ufw/applications.d/jriver```
```
[jriver]
title=jriver
description=Ports required by JRiver MediaCenter
ports=5800,52199,52100,52101,5900/tcp|1900/udp
```
3. Update the app profile ```ufw app update jriver``` after you can check your configuration with ```ufw app info jriver```
4. Allow the app ```ufw allow jriver```

# Two Factor Authentication

The web ui supports TOTP (Time-based one-time password) 2FA. This will work with apps such as Google Authenticator, Authy or Microsoft Authenticator. If you use another app that supports TOTP it should work too. To enable 2FA, create a new code in the profile page and scan the QR code with your 2FA phone app. Afterwards you can enable 2FA. It won´t let you enable 2FA for your user if you don´t have at least one 2FA method added.

When you loose access to your phone you can temporarily disable 2FA globally by starting the container with ```DISABLE_2FA``` set to ```1```. 

# Regenerate Jwt Secret

At first startup a file is created at ```/config/appstorage/.secret```. This file contains a randomly generated sequence that is used to generate and validate jwt tokens.

Removing this file and restarting the container will generate a new one.

You might want to do this if you want to force logout everyone.

# Run behind Reverse Proxy

If you are running a central NGINX server as reverse proxy you can use these configurations as a starting point.

## NGINX

This is a configuration [similar to the one the container uses](https://gitlab.com/shiomax/jrivermc-docker/-/raw/main/rootfs/etc/nginx/sites-available/http_config).

```nginx
upstream mediacenter {
  # Docker container running mediacenter
  server 127.0.0.1:5800;
}

# Redirect HTTP connections to HTTPS
#server {
#    listen       80;
#    listen  [::]:80;

#    return 301 https://$host$request_uri;
#}

server {
    listen 443 ssl;

    ssl_certificate /path/to/cert;
    ssl_certificate_key /path/to/private_key;

    location / {
        proxy_pass http://mediacenter;
        proxy_http_version 1.1;
    }

    location /api {
        proxy_pass http://mediacenter;
        proxy_http_version 1.1;
        proxy_set_header Upgraded $http_upgrade;
        proxy_set_header Connection keep-alive;
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /websockify {
        proxy_pass http://mediacenter;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $host;

        # VNC connection timeout
        proxy_read_timeout 61s;

        # Disable cache
        proxy_buffering off;
    }
}
```

Replace ```server 127.0.0.1:5800;``` with the IP of the container and fix up paths for ```ssl_certificate``` and ```ssl_certificate_key```.

# Image Tags

Images are currently available for ```arm64``` and ```amd64```. Tags are pushed in various formats.

| Example Tag | Description | MC Source |
| ----------- | ----------- | ------------------- |
| ```latest``` | Using latest version of MC from the ```latest``` apt repository. | ```apt repository```
| ```latest-31``` | Will not upgrade past MC ```31```. | ```apt repository```
| ```latest-31-v0.3.1``` | Specify image version to stay at image version ```v0.3.1```. | ```apt repository```
| ```latest-31.0.54-v0.3.1``` | Tag specifies MC minor version. These are using .deb files and not the repository. | ```.deb file```

All tags above are also available with ```-amd64``` and ```-arm64``` suffix. The tags without are multiarch images that refer to images for specific architectures. Docker should figure out what image to pull on it's own without specifying, but if you are having trouble you can specify which architecture you want to pull.

# Image Versioning

Version numbers ```MAJOR.MINOR.PATCH``` mean the following

- ```MAJOR``` will go unused for quite some time until ```v1.0.0``` which would indicate feature completion
- ```MINOR``` new or improved features in one way or another
- ```PATCH``` bugfix, library updates

New mediacenter versions may be added without any version changes to the image, if no changes to the image where necessary to make it work.

# Environment Variables

| Variable | Description | Default |
|----------| ------------| --------|
| ```USER_ID``` | ID of the user the application runs as. | ```1000``` |
| ```GROUP_ID```| ID of the group the application runs as. | ```1000``` |
| ```SUP_GROUP_IDS``` | Supplementary comma seperated list of additional group ids for MC. | ```<unset>``` |
| ```DISPLAY_WIDTH``` | Initial width (in pixels) of the application's window. | ```1280``` |
| ```DISPLAY_HEIGHT``` | Initial height (in pixels) of the application's window. | ```768``` |
| ```TZ``` | [TimeZone] of the container.  Timezone can also be set by mapping ```/etc/localtime``` between the host and the container. | ```Etc/UTC``` |
| ```TAKE_CONFIG_OWNERSHIP``` | When set to ```1```, owner and group of ```/config``` (including all its files and subfolders) are automatically set during container startup to ```USER_ID``` and ```GROUP_ID``` respectively. | ```1``` |
| ```CLEAN_TMP_DIR``` | When set to ```1```, all files in the ```/tmp``` directory are delete during the container startup. | ```1``` |
| ```SECURE_CONNECTION``` | When set to ```1``` a self signed SSL certificate will be generated to enable ```HTTPS```| ```1``` |
| ```VNC_LISTEN``` | Setting the value to ```LOCALHOST``` will allow connections from localhost only. Setting it to ```ALLOW_ALL``` will allow all connections. By default VNC will not listen on any port. | ```<unset>``` |
| ```STATELESS_MODE``` | When set to ```1``` the jriver directory ```/config/.jriver``` will be copied to a temporary directory on startup. **Do not** enable this for regular installs. It´s ment for testing other apps that use MCWS. Restarting the container with ```STATELESS_MODE``` enabled will reset the library to what it was before ```STATELESS_MODE``` was enabled. | ```0```
| ```INSTALL_INTEL_GPU_DRIVER``` | By default if an intel cpu is detected and the devices where passed through the driver for intel GPUs will be installed. This can be overridden by setting this to ```1``` (to install) or ```0``` (to never install) the driver. | ```<unset>```
| ```INSTALL_BROWSER``` | When set to ```1``` will install ```firefox-esr```. While mc has an integrated browser, there are some links that will not open unless there is a browser installed on the system. | ```<unset>``` |  
| ```DISABLE_AUTH``` | When set to ```1``` authentication will be disabled for the web ui. | ```<unset>```
| ```JWT_TOKEN_LIFETIME``` | Sets lifetime of jwt auth tokens. Default value corresponds to 5 minutes. It´s unlikely that you`d have to change this value. But if you need you can. Details about the formats that are valid can be found [here](https://learn.microsoft.com/en-us/dotnet/standard/base-types/standard-timespan-format-strings). | ```00:05:00``` |
| ```REFRESH_TOKEN_LIFETIME``` | Sets lifetime for refresh tokens. This will impact how long your login stays active for (when **not** actively in use). Default value corresponds to 2 days. Changing this value will not have an effect on existing tokens / logins that happened before changing the value. Details about the formats that are valid can be found [here](https://learn.microsoft.com/en-us/dotnet/standard/base-types/standard-timespan-format-strings). | ```2.00:00:00``` |
| ```CERTMONITOR_INTERVAL``` | Inverval in seconds that certmonitor will check for certificate expiery. Default is equivalent to once a week. This also effects when certs are considered expired. Certificates are regenerated when there is less than ```CERTMONITOR_INTERVAL``` lifetime left. | ```604800``` |
| ```DISABLE_2FA``` | When set to ```1``` Two Factor authentication will be disabled globally. When loosing access to your two factor device(s) you can disable it to restore access. | ```<unset>```
| ```MC_STARTUP_OPTIONS``` | Options used to start MC. | ```/MediaServer```

# Changelog

### v0.4.6
- Update various dependencies
- Update API to .net 9
- Websockify now using python venv

### v0.4.5
- ```INSTALL_BROWSER``` will now configure ```/usr/bin/firefox``` as default browser
- Install ```libnss3``` to fix internal browser on arm builds (already was installed on amd64 automatically)
- Update various dependencies

### v0.4.4
- Fix issue with container failing to start after being stopped once (typo in ```usermod```)

### v0.4.3
- Add back chromium dependencies for mc32. Webkit is only ment for 32bit installs, but chromium won´t work either without ```libwebkit2gtk``` appearently
- Change baseimage source to come from aws instead of dockerhub (this should not change anything, it´s to dodge rate limits)
- Fix app user was not part of app group, dont create ```abc``` group

### v0.4.2
- API is now AOT compiled (reduced image size)
- Add ```MC_STARTUP_OPTIONS``` to overrride MC startup options (defaults to ```/MediaServer``` same as before)
- Add MC32 builds and MC31/30 stable versions
- Install ```libwebkit2gtk``` for mc32 and up (internal browser changed)

### v0.4.1
- Using debian 12 as baseimage
- TOTP code input now works with touch inputs and you can paste the code
- Fix builds using jriver repository did not work anymore

### v0.4.0
- Change ```SECURE_CONNECTION``` default to ```1``` because novnc really wants a SSL certificate.
- Self signed certificates are now checked for renewal once a week, no container restart is required anymore when the certs expires
- HTTP to HTTPS redirection now redirects to the port entered instead of always to ```5800```
- Some backend changes, mostly to make it compatible with native aot support coming for APIs in .net 8 later this year. Some of these changes are not backwards compatible. If you want to downgrade to ```v0.3.1``` or earlier version you´ll have to delete the database file in ```/config/appstorage/db.sqlite```. Upgrading should work fine without any extra steps.
- Support for TOTP (Time-based one-time password) 2FA

### v0.3.1
- Fix on first visit with expired tokens you´d look at a black screen, should have navigated to ```/login``` but that didn´t work
- Update library versions

### v0.3.0
- Add cdrom device group to app user when exposed to container
- Hide the errors prevent-minimize script prints on container start before display is initialized
- Navigating to ```/login``` when already logged in now redirects to ```/```
- When setting ```INSTALL_BROWSER``` to ```1``` will install firefox on startup (last.fm accounts page is still not opening though)
- Disable ```S6_CMD_WAIT_FOR_SERVICES_MAXTIME``` to prevent startup script from being killed when they take too long

### v0.2.2
- Fix some nginx logs where written to ```/config/log/nginx``` instead of ```/log/nginx```
- SSL Certificate generated when ```SECURE_CONNECTION``` flag is set will regenerate on container start when there is less than 7 days left of its lifetime

### v0.2.1
- Fix ```DISABLE_AUTH``` breaking novnc in the webui when using ```VNC_LISTEN``` settings ```LOCALHOST``` or ```ALLOW_ALL```

### v0.2.0
- Add option to disable authentication by setting ```DISABLE_AUTH``` to ```1``` on container creation
- Minor UI updates
- Update library versions

### v0.1.0
- Change window manager to ```jwm```
- Update ```prevent-minimize``` script to work with ```jwm```
- Patch websockify to support listening to unix sockets (port ```6080``` is no longer in use)
- Update ```jrivermc-docker-api``` to use .net 7

### v0.0.1
- Initial Release
