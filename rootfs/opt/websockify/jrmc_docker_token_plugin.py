import sys
import jwt
import os

class JRMCDockerTokenPlugin:

    def __init__(self, src):
        # JWT secret passed with --token-source
        self.source = src
        # Unix socket is used when VNC access is disabled
        vnclisten = os.environ.get('VNC_LISTEN', default = None)
        self.websocket = vnclisten != "LOCALHOST" and vnclisten != "ALLOW_ALL"

    def lookup(self, token):
        try:
            jwt.decode(token, self.source, algorithms=["HS256"], options={ "require": [ "exp", "nbf", "id" ]})

            return ("unix_socket", "/tmp/tigervnc.sock") if self.websocket else ("127.0.0.1", 5900)
        except jwt.exceptions.ExpiredSignatureError:
            print('Token \'%s\' has expired!' % token, file=sys.stderr)
            return None
        except jwt.exceptions.InvalidSignatureError:
            print('Signature is invalid!', file=sys.stderr)
            return None
        except Exception as e:
            print("Token not valid: %s" % e, file=sys.stderr)
            return None
